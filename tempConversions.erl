-module(tempConversions).
-export([f2c/1, c2f/1]).

f2c(X) ->
	conversionF(X).

conversionF(X) ->
		5*(X-32)/9.
c2f (X)->
	conversionC(X).
conversionC(X) ->
		(9*X/5)+32.

