-module(lists3).
-export([maxMin/1]).

maxMin(List)->
		{min(List),max(List)}.


max([X|Rest])->
		max(X,Rest).

max(M,[]) ->M;
max(M,[X|Rest]) when  X> M->
			max(X,Rest);
max(M,[_|Rest]) ->
			max(M,Rest).
			min([X|Rest])->
		min(X,Rest).

min(M,[]) ->M;
min(M,[X|Rest]) when  X< M->
			min(X,Rest);
min(M,[_|Rest]) ->
			min(M,Rest).
