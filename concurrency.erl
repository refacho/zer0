-module(concurrency).
-compile(export_all).

start()->
	spawn(?MODULE,message,[]).

message()->
		receive
			{From,Message}->
				From ! {self(),Message}
		after 0->
			timeout
		end,
		message().
