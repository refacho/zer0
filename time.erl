-module(time).
-export([swedishDate/0]).

swedishDate()->
		swedishDate(date()).

swedishDate({A,B,C}) when C<10,B>10->
		A*1000+B*10+C;
swedishDate({A,B,C}) when C<10,B<10 ->
		A*100+B*10+C;
swedishDate({A,B,C}) when C>10,B<10->
		A*1000+B*100+C;
swedishDate({A,B,C}) ->
		A*10000+B*100+C.



