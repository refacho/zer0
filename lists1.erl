-module(lists1).
-export([min/1]).

min([X|Rest])->
		min(X,Rest).

min(M,[]) ->M;
min(M,[X|Rest]) when  X< M->
			min(X,Rest);
min(M,[X|Rest]) ->
			min(M,Rest).

