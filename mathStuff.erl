-module(mathStuff).
-export([perimeter/1]).

perimeter([square,X]) ->
			[square,sum(X)];

perimeter([circle,X]) ->
			2*math:pi()*X;

perimeter([triangle,A,B,C])->
			A+B+C.

sum(X)->
	X*4.







