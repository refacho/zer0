-module(shapes).
-export([area/1]).

%exercise from erlang book

area({triangle,A,B,C}) -> S = (A+B+C)/2,
			math:sqrt(S*(S-A)*(S-B)*(S-C)).
