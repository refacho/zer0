-module(listMax).
-export([max/1]).

max([X|Rest])->
		max(X,Rest).

max(M,[]) ->M;
max(M,[X|Rest]) when  X> M->
			max(X,Rest);
max(M,[_|Rest]) ->
			max(M,Rest).
